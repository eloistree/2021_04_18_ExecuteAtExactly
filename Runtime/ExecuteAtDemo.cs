﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public class ExecuteAtDemo : MonoBehaviour
{
    public bool m_useDemo;
    public ExacteExecuterManagerMono m_executorThread;

    public float m_triggerAction = 4;
    public Action m_todo;
    public float m_triggerUnityEvent = 8;
    public UnityEvent m_toDoBeCarefull;

    private void Awake()
    {
        if (m_useDemo) { 
            m_todo = new Action(ComputeRandomNumber);
            m_toDoBeCarefull.AddListener(ComputeRandomNumber);

            for (int i = 0; i < 5000; i++)
            {
                m_executorThread.DoExecutionAt(ComputeRandomNumber, DateTime.Now.AddSeconds(i), "" + i);
            }
            m_executorThread.DoActionAt(m_todo, DateTime.Now.AddSeconds(m_triggerAction), "Action");
            m_executorThread.DoExecutionAt(m_toDoBeCarefull, DateTime.Now.AddSeconds(m_triggerUnityEvent), "Unity Event");
        }
    }


    private void ComputeRandomNumber()
    {
        int a = 0;
        for (int i = 0; i < 500000; i++)
        {
            a += 5;
        }
    }

    public void SleepTestTask(int time)
    {
        Thread.Sleep(time);
    }
    public GameObject m_prefab;
    public void CreatePrefab(int count)
    {
        for (int i = 0; i < count; i++)
        {
                GameObject gmo=null;
            if (m_prefab)
                gmo= GameObject.Instantiate(m_prefab);
            else gmo = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            gmo.transform.position = Vector3.zero;
            gmo.transform.rotation = Quaternion.Euler( UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);

        }
    }
}

[System.Serializable]
public class UnityThreadActionAt : TryToExecuteDirectlyAt
{

    public int m_inWaitingCall = 0;
    I_TryToExecuteAt m_inWaitingAction;

    public UnityThreadActionAt(I_TryToExecuteAt toExecute)
        : base(toExecute.GetExacteDate(), null, toExecute.GetHumanReminder())
    {
        SetToExecute(AddCall);
        m_inWaitingAction = toExecute;
    }

    public void AddCall()
    {
        m_inWaitingCall++;
    }
    public bool HasWaitingCall()
    {
        return m_inWaitingCall > 0;
    }
    public void ExecuteWaitingCall()
    {
        if (!HasWaitingCall())
            return;
        for (int i = 0; i < m_inWaitingCall; i++)
        {
            if (m_inWaitingAction != null) { 
                m_inWaitingAction.DoTheThing();
                m_inWaitingAction.ComputedConclusion();
            }
        }
    }
   
}
