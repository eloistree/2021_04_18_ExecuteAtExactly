﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CallTimeTesterMono : MonoBehaviour
{
    public ExacteExecuterManagerMono m_executer;
    public UnityEvent m_toTest;
     UnityThreadActionAt m_actionCall;
    public TryToExecuteDirectlyAt m_action;

    [ContextMenu("Play test")]
    public void Play() {
        m_action = new TryToExecuteDirectlyAt(DateTime.Now.AddSeconds(1), m_toTest.Invoke, "Just quick test");
        m_actionCall = ExacteExecuterManagerMono.CreateUnityEventExecuter(m_action);
        m_executer.DoAsSoonAsPossibleUnityActionAt(m_actionCall);

    }


    private void Reset()
    {
        m_executer= GameObject.FindObjectOfType<ExacteExecuterManagerMono>();
    }
}
