﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ExacteExecuterManagerMono : MonoBehaviour
{
    public ThreadExecuteAtPreciselyMono m_threadExecutor;
    public TryToExecuteDirectlyAt m_debugDirectPreviouslyDone;
    public UnityThreadActionAt m_debugUnityPreviouslyDone;
    public List<UnityThreadActionAt> m_inWaitingUnityAction= new List<UnityThreadActionAt>();

    private void Awake()
    {
        m_threadExecutor.AddDoneListener(NotifyExecuteDone);
    }

    private void NotifyExecuteDone(ref I_TryToExecuteAt done)
    {
        done.ComputedConclusion();
        if (done is TryToExecuteDirectlyAt) {
            m_debugDirectPreviouslyDone = (TryToExecuteDirectlyAt) done;
        }
        if (done is UnityThreadActionAt) {
            
            m_inWaitingUnityAction.Add((UnityThreadActionAt) done);
            m_debugUnityPreviouslyDone = (UnityThreadActionAt)done;
        }
    }

    private void Update()
    {
        if (m_inWaitingUnityAction.Count > 0) {
            for (int i = 0; i <m_inWaitingUnityAction.Count; i++)
            {
                m_inWaitingUnityAction[i].ExecuteWaitingCall();
            }
            m_inWaitingUnityAction.Clear();
        }
    }


    public void DoActionAt(Action toDo, DateTime when, string reminder = "")
    {
        m_threadExecutor.Enqueue(CreateActionExecuter(toDo, when, reminder));
    }

    public void DoExecutionAt(ToBeExecuted toDo, DateTime when, string reminder = "")
    {
        m_threadExecutor.Enqueue(CreateDeletateExecuter(toDo, when, reminder));
    }
    public void DoExecutionAt(UnityEvent toDo, DateTime when, string reminder = "")
    {
        m_threadExecutor.Enqueue(new UnityThreadActionAt(CreateDeletateExecuter(toDo.Invoke, when, reminder)));
    }
    public void DoAsSoonsAsPossibleUnityActionAt(ToBeExecuted toDo, DateTime when, string reminder = "")
    {
        m_threadExecutor.Enqueue(new UnityThreadActionAt(CreateDeletateExecuter(toDo, when, reminder)));
    }
    public void DoAsSoonsAsPossibleUnityActionAt(Action toDo, DateTime when, string reminder = "")
    {
        m_threadExecutor.Enqueue(new UnityThreadActionAt(CreateActionExecuter(toDo, when, reminder)));
    }
    public void DoAsSoonAsPossibleUnityActionAt(UnityThreadActionAt toDo)
    {
        m_threadExecutor.Enqueue(toDo);
    }

    public static I_TryToExecuteAt CreateActionExecuter(Action toDo, DateTime when, string reminder)
    {
        return new TryToExecuteDirectlyAt(when, toDo.Invoke, reminder);
    }
    public static I_TryToExecuteAt CreateDeletateExecuter(ToBeExecuted toDo, DateTime when, string reminder)
    {
        return new TryToExecuteDirectlyAt(when, toDo, reminder);
    }
    public static UnityThreadActionAt CreateUnityEventExecuter(UnityEvent toDo, DateTime when, string reminder)
    {
        return new UnityThreadActionAt(CreateDeletateExecuter(toDo.Invoke, when, reminder));
    }
    public static UnityThreadActionAt CreateUnityEventExecuter(I_TryToExecuteAt toDo)
    {
        return new UnityThreadActionAt(toDo);
    }


    //   public float m_preEnqueueTime = 1000;
    //public void CheckEverySecondToEnqueue() {
    //    DateTime threadTime = m_executorThread.GetThreadTime();
    //    // Manage to on this thread package that soon need to be execute vs later 
    //    // The only goal of this class is to avoid computation on the exact thread. 
    //    throw new NotImplementedException();
    //}
}


public interface I_TryToExecuteAt
{

    void SetStartDate(DateTime start);
    void SetEndDate(DateTime start);
    bool IsComputedConclusion();
    void ComputedConclusion();
    void DoTheThing();
    void SetWith(DateTime exectDate, ToBeExecuted toDo, string humainReminder);
    DateTime GetExacteDate();
    string GetHumanReminder();
}



public delegate void ToBeExecuted();

public abstract class TryToExecuteAt : I_TryToExecuteAt
{

    [SerializeField] string m_humainReminderText;
    protected ToBeExecuted m_toExecute;
    public DateTime m_exactDate;
    DateTime m_startedToExecute;
    DateTime m_startedFinishToExecute;

    public int m_missedMillisecondsPecision;
    public int m_timeToExecuted;
    public bool m_conclusionComputed;

    public TryToExecuteAt(DateTime exactDate, ToBeExecuted toExecute, string humainReminder = "")
    {
        SetWith(exactDate, toExecute, humainReminder);
    }

    public void SetStartDate(DateTime start) => m_startedToExecute = start;
    public void SetEndDate(DateTime end)=> m_startedFinishToExecute = end;

    public void ComputedConclusion()
    {
           m_missedMillisecondsPecision = (int)(m_startedToExecute - m_exactDate).TotalMilliseconds;
           m_timeToExecuted = (int)(m_startedFinishToExecute - m_startedToExecute).TotalMilliseconds;
           m_conclusionComputed = true;
        
    }
    public abstract void DoTheThingChildDo();

    public void SetWith(DateTime exectDate, ToBeExecuted toDo, string humainReminder)
    {
        m_humainReminderText = humainReminder;
        m_toExecute = toDo;
        m_exactDate = exectDate;
    }

    public DateTime GetExacteDate()
    {
        return m_exactDate;
    }

    public void DoTheThing()
    {
       SetStartDate(DateTime.Now);
       DoTheThingChildDo();
       SetEndDate(DateTime.Now);
    }

    public bool IsComputedConclusion()
    {
        return m_conclusionComputed ;
    }

    public string GetHumanReminder()
    {
        return m_humainReminderText;
    }
}
// ME TRYING TO MAKE A UNITY THREAD SAVE VERSION BUT REALIZEING THAT A BAD IDEA
//public class TryToExecuteOnUnityThreadAt : TryToExecuteAt
//{
//    public TryToExecuteOnUnityThreadAt(DateTime exactDate, ToBeExecuted toExecute, string humainReminder = "")
//        : base(exactDate, toExecute, humainReminder)
//    {
//    }
//    bool m_isReadyToBexecuted;
//    bool m_hasBeenExecuted;
//    public override void DoTheThing()
//    {
//        m_isReadyToBexecuted = true;
//    }
//    public bool IsReadyToBeExecuted() {
//        return m_isReadyToBexecuted;
//    }

//    public void CodeToBeExecuted() {

//        if (!IsReadyToBeExecuted()) 
//            return;
//        if (!m_hasBeenExecuted) {
//            m_hasBeenExecuted = true;
//            if (m_toExecute != null) 
//                m_toExecute(); 
//        }
//    }
//}
[System.Serializable]
public class TryToExecuteDirectlyAt : TryToExecuteAt
{
    public TryToExecuteDirectlyAt(DateTime exactDate, ToBeExecuted toExecute, string humainReminder = "") 
        : base(exactDate, toExecute, humainReminder)
    {
    }
    public void SetToExecute(ToBeExecuted toExecute) {
        m_toExecute = toExecute;
    }
    public override void DoTheThingChildDo()
    {
            if (m_toExecute != null)
                m_toExecute();
    }
}