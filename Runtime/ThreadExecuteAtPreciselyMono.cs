﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ThreadExecuteAtPreciselyMono : MonoBehaviour
{
    public double m_threadSleepPrecision = 1;
    public System.Threading.ThreadPriority m_priority;
    [Space]
    [Header("Debug view")]
    [Tooltip("Use to kill the thread if needed")]
    public bool m_threadAlive;
    [Tooltip("Time the thread is really alive")]
    public double m_millisecondsReallyPast;
    [Tooltip("How many milliseconds has based between thread.sleep")]
    public double m_timePastBetweenCheck;

    [Tooltip("Time checked by the thread")]
    public double m_millisecondsPast;
    [Tooltip("Time where impression can be done because of Thread called too late by the system")]
    public double m_millisecondsMissed;
    [Range(0.0f, 1.0f)]
    [Tooltip("Pourcent of missed time")]
    public double m_pourcentLost;


    private double m_timePastCumulation;
    public List<I_TryToExecuteAt> m_toExecute = new List<I_TryToExecuteAt>();
    public DateTime threadStart;
    public DateTime start;
    public DateTime previous, current;

    public DateTime GetThreadTime()
    {
        return current;
    }

    public void Enqueue(ref I_TryToExecuteAt tryToExecuteAt)
    {
        m_toExecute.Add(tryToExecuteAt);
    }
    public void Enqueue(I_TryToExecuteAt tryToExecuteAt)
    {
        m_toExecute.Add(tryToExecuteAt);
    }



    void Start()
    {
        current = previous = DateTime.Now;
        m_threadAlive = true;
        Thread t = new Thread(CheckTime);
        t.Priority = m_priority;
        t.Start();
    }
    private void OnDestroy()
    {
        m_threadAlive = false;
    }

    private void CheckTime()
    {
        threadStart = DateTime.Now;
        while (m_threadAlive)
        {
            current = DateTime.Now;
            m_millisecondsReallyPast = (current - threadStart).TotalMilliseconds;
            m_timePastCumulation+= (current - previous).TotalMilliseconds;
            m_timePastBetweenCheck = m_timePastCumulation;

            if (m_timePastCumulation > m_threadSleepPrecision)
            {
                if (m_timePastCumulation > m_threadSleepPrecision)
                {
                    m_millisecondsMissed += m_timePastCumulation - m_threadSleepPrecision;

                }
                m_millisecondsPast += m_timePastCumulation;
                m_pourcentLost = m_millisecondsMissed / m_millisecondsPast;

                m_timePastCumulation = 0;
            }
            if (m_toExecute.Count > 0)
            {
                for (int i = m_toExecute.Count - 1; i >= 0; i--)
                {
                    if (m_toExecute[i].GetExacteDate() >= previous && m_toExecute[i].GetExacteDate() <= current)
                    {

                        Execute(m_toExecute[i]);
                        NotifyAsDone(m_toExecute[i]);
                        m_toExecute.RemoveAt(i);
                    }
                }
            }

            previous = current;
            Thread.Sleep((int)m_threadSleepPrecision);
        }
    }
    //private void CheckTime()
    //{
    //    while (m_threadAlive) {
    //        current = DateTime.Now;

    //        uint timePastInInt = (uint)(current - previous).TotalMilliseconds;
    //        if (timePastInInt > 0) {
    //            if (timePastInInt > 1)
    //            {
    //                m_millisecondsMissed += timePastInInt - 1;

    //            }
    //            m_millisecondsPast += timePastInInt;
    //            m_pourcentLost = (double)m_millisecondsMissed / (double)m_millisecondsPast;

    //            if (m_toExecute.Count > 0) {
    //                for (int i = m_toExecute.Count - 1; i >= 0; i--)
    //                {
    //                    if (m_toExecute[i].GetExacteDate() >= previous && m_toExecute[i].GetExacteDate() <= current) {

    //                        Execute(m_toExecute[i]);
    //                        NotifyAsDone(m_toExecute[i]);
    //                        m_toExecute.RemoveAt(i);
    //                    }
    //                }
    //            }

    //            previous = current;
    //        }
    //        Thread.Sleep(1);
    //    }
    //}

    private void NotifyAsDone(I_TryToExecuteAt tryToExecuteAt)
    {
        if(m_notifiedAsDone != null)
        m_notifiedAsDone(ref tryToExecuteAt);
    }

    private void Execute(I_TryToExecuteAt tryToExecuteAt)
    {
        tryToExecuteAt.DoTheThing();
    }

    public delegate void Executed(ref I_TryToExecuteAt done);
    public Executed m_notifiedAsDone;
    public void AddDoneListener(Executed listener)
    {
        m_notifiedAsDone += listener;
    }
    public void RemoevDoneListener(Executed listener)
    {
        m_notifiedAsDone -= listener;
    }
}
